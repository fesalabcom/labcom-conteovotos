-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 07-02-2018 a las 16:41:18
-- Versión del servidor: 5.7.21-0ubuntu0.16.04.1
-- Versión de PHP: 5.6.33-3+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `prep`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acta_por_casillas`
--

CREATE TABLE `acta_por_casillas` (
  `id_casilla` int(11) NOT NULL,
  `votos_esperados` float NOT NULL COMMENT 'Numero de personas esperadas a votar en esa casilla',
  `legible` tinyint(1) NOT NULL,
  `contabilizada` tinyint(1) NOT NULL,
  `votos_nulos` int(11) DEFAULT NULL COMMENT 'Cuando un acta tiene votos nulos por capturar'
) ENGINE=InnoDB ;

--
-- Volcado de datos para la tabla `acta_por_casillas`
--

INSERT INTO `acta_por_casillas` (`id_casilla`, `votos_esperados`, `legible`, `contabilizada`, `votos_nulos`) VALUES
(1, 50, 1, 1, 5),
(2, 50, 1, 1, 0),
(3, 50, 1, 0, 0),
(4, 25, 1, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `candidatos`
--

CREATE TABLE `candidatos` (
  `id_candidato` int(11) NOT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  `p_apellido` varchar(100) DEFAULT NULL,
  `s_apellido` varchar(100) DEFAULT NULL,
  `id_partido` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB ;

--
-- Volcado de datos para la tabla `candidatos`
--

INSERT INTO `candidatos` (`id_candidato`, `nombre`, `p_apellido`, `s_apellido`, `id_partido`, `activo`) VALUES
(1, 'Ulises', 'de la cruz', 'escamilla', 1, 1),
(2, 'Ariadne Ivette', 'Olarte', 'Cetina', 2, 1),
(3, 'ISAAC', 'VICTORIA', 'JURADO', 3, 1),
(4, 'MARCO', 'ANTONIO', 'REGIL', 4, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_accesos`
--

CREATE TABLE `c_accesos` (
  `id_acceso` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL
) ENGINE=InnoDB  COMMENT='Catalogo para nombrar tipoos de acceso, como Administrador o Capturista.';

--
-- Volcado de datos para la tabla `c_accesos`
--

INSERT INTO `c_accesos` (`id_acceso`, `nombre`) VALUES
(0, 'admin'),
(1, 'capturista');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `c_partidos`
--

CREATE TABLE `c_partidos` (
  `id_partido` int(11) NOT NULL,
  `nombre` varchar(150) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB ;

--
-- Volcado de datos para la tabla `c_partidos`
--

INSERT INTO `c_partidos` (`id_partido`, `nombre`, `activo`) VALUES
(1, 'PARTIDO UNO (PU)', 1),
(2, 'PARTIDO DOS (PD)', 1),
(3, 'PARTIDO TRES (PT)', 1),
(4, 'PARTIDO CUATRO (PC)', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `planilla`
--

CREATE TABLE `planilla` (
  `id_planilla` int(11) NOT NULL,
  `id_candidato` int(11) NOT NULL,
  `id_casilla` int(11) NOT NULL,
  `votos` int(11) DEFAULT NULL,
  `con_datos` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB ;

--
-- Volcado de datos para la tabla `planilla`
--

INSERT INTO `planilla` (`id_planilla`, `id_candidato`, `id_casilla`, `votos`, `con_datos`) VALUES
(1, 1, 1, 5, 1),
(2, 1, 2, 6, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `contrasenia` varchar(255) NOT NULL,
  `acceso_inicio` datetime DEFAULT NULL,
  `acceso_fin` datetime DEFAULT NULL,
  `id_tipo_acceso` int(11) NOT NULL,
  `activo` tinyint(1) NOT NULL
) ENGINE=InnoDB  COMMENT='Los usuarios que tendran acceso a la plataforma, tendrán tiempos de vida para sus accesos,\ny el tipo de acceso al sistema.';

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id_usuario`, `usuario`, `contrasenia`, `acceso_inicio`, `acceso_fin`, `id_tipo_acceso`, `activo`) VALUES
(5, 'ulises.escamilla', '$2y$10$H38SYGW88W477rU85GNiDes4k0ctDwBoaPWp82XxQvuZN1V9Za3AK', NULL, NULL, 0, 1),
(6, 'unam', '$2y$10$h8BuJmV1LGMW3nfx83EV7eHOEt6i1z5IooxX4JZugVYUpK3/FBZMG', NULL, NULL, 0, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acta_por_casillas`
--
ALTER TABLE `acta_por_casillas`
  ADD PRIMARY KEY (`id_casilla`);

--
-- Indices de la tabla `candidatos`
--
ALTER TABLE `candidatos`
  ADD PRIMARY KEY (`id_candidato`),
  ADD KEY `fk_candidatos_partidos1_idx` (`id_partido`);

--
-- Indices de la tabla `c_accesos`
--
ALTER TABLE `c_accesos`
  ADD PRIMARY KEY (`id_acceso`);

--
-- Indices de la tabla `c_partidos`
--
ALTER TABLE `c_partidos`
  ADD PRIMARY KEY (`id_partido`);

--
-- Indices de la tabla `planilla`
--
ALTER TABLE `planilla`
  ADD PRIMARY KEY (`id_planilla`),
  ADD KEY `fk_planilla_candidatos1_idx` (`id_candidato`),
  ADD KEY `fk_planilla_casillas1_idx` (`id_casilla`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id_usuario`),
  ADD KEY `fk_usuarios_c_accesos1_idx` (`id_tipo_acceso`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `candidatos`
--
ALTER TABLE `candidatos`
  MODIFY `id_candidato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `c_accesos`
--
ALTER TABLE `c_accesos`
  MODIFY `id_acceso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `c_partidos`
--
ALTER TABLE `c_partidos`
  MODIFY `id_partido` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `planilla`
--
ALTER TABLE `planilla`
  MODIFY `id_planilla` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id_usuario` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `candidatos`
--
ALTER TABLE `candidatos`
  ADD CONSTRAINT `fk_candidatos_partidos1` FOREIGN KEY (`id_partido`) REFERENCES `c_partidos` (`id_partido`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `planilla`
--
ALTER TABLE `planilla`
  ADD CONSTRAINT `fk_planilla_candidatos1` FOREIGN KEY (`id_candidato`) REFERENCES `candidatos` (`id_candidato`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_planilla_casillas1` FOREIGN KEY (`id_casilla`) REFERENCES `acta_por_casillas` (`id_casilla`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD CONSTRAINT `fk_usuarios_c_accesos1` FOREIGN KEY (`id_tipo_acceso`) REFERENCES `c_accesos` (`id_acceso`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
