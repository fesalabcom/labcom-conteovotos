<?php
/**
 * Encabezado de las páginas para corroborar acceso 
 * y tipo de acceso, por ejemplo si es Administrador
 * o Capturista
 * 
 * @author Ulises Escamilla
 * @version 1.0.0
 * @category session
 */
session_start();
// Si está seteada la sesión corroboramos tipo de acceso
if (isset($_SESSION)){

    // Validar permisos
    if (isset($_SESSION['tipo_usuario']) && isset($_SESSION['login_user'])){
        $tipo_acceso = $_SESSION['tipo_usuario'];
        switch ($tipo_acceso) {
            case '0':
                # Tipo de acceso de administrados
                echo "Bienvenido: ".$_SESSION['login_user']." ";
                echo "Administrador";
                break;
            
            default:
                # Capturista por defecto
                echo "Bienvenido: ".$_SESSION['login_user']." ";
                echo "Capturista";
                break;
        }
    }
    else{
        header("location: login.php");
    }

}else{
    header("location: login.php");
}
?>