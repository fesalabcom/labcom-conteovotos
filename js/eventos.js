function marcar_ilegible(){
    var hiddenInput = $('#legible');
    var formulario  = $("#contenedor");
     formulario.find("input.form-control").prop('disabled', true);
     formulario.find("input.checkbox").prop('disabled',true);
     formulario.find("input.checkbox").prop('checked',false);
     hiddenInput.val('0');
    }
    function marcar_legible(){
        var hiddenInput = $('#legible');
        var formulario  = $("#contenedor");
         formulario.find("input.form-control").prop('disabled', false);
         formulario.find("input.checkbox").prop('disabled',false);
         hiddenInput.val('1');
        }
function marcar_checkbox(idCheckbox){

    var idInput = "candidato"+idCheckbox;
    idInput = "#"+idInput;
    var inputCandidato = $( idInput );
    if( $("#"+idCheckbox).prop('checked') ) {
        inputCandidato.prop('disabled', true);
    } else{ 
        inputCandidato.prop('disabled', false);
    }
}
