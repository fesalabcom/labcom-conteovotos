// IIFE - Immediately Invoked Function Expression
(function (main) {



  // The global jQuery object is passed as a parameter
  main(window.jQuery, window, document);


}(function ($, window, document) {

  var total_actas = 0;

  $(window).bind("load", function () {
    var opcion = {};
    opcion["peticion"] = "total_actas";
    $.ajax({
      url: "conteo.php",
      type: "get",
      data: opcion,
      success: function (data) {
        total_actas = data;
      }
    });

    var barraProgreso = $("#progress-bar");
    barraProgreso.attr("class", "progress-bar progress-bar-striped active");
    barraProgreso.attr("style", "width:100%");
    $("#progreso").text("Cargando. . .");

  });

  // The $ is now locally scoped

  // Listen for the jQuery ready event on the document
  $(function () {
    // The DOM is ready!
    var tiempo = 1000 * 60 * 5; // Cinco minutos aprox
    var tiempo_inicio = 2500;

    // Elementos
    var btnEnviar = $("#btnEnviar");
    var barraProgreso = $("#progress-bar");
    var btnIlegible = $('#btnIlegible');


    // Cambiar estilo a "Cargando..."
    barraProgreso.attr("class", "progress-bar active");
    barraProgreso.attr("style", "width:10%");



    // Variables
    var capturados = 0;
    var progreso = 0;
    // Inicializa el timer
    setTimeout(actualizaProgreso, tiempo_inicio);


    //getTotalActas();
    actualizaProgreso();

    /**
     * Eventos
     */
     var status=true;
    btnIlegible.on("click", function() {

      if(status==true){//bloquear
        marcar_ilegible();
        status=false;
      }else{ //desbloquear
        marcar_legible();
        status=true;
      }

    });

    $(":checkbox").on("click", function(){
      var idCandidato = this.id;
          marcar_checkbox(idCandidato);


    } );


    /**
     * Petición de la barra de progreso
     */
    function actualizaProgreso() {

      var opcion = {};
      opcion["peticion"] = "capturados";

      progreso = capturados * 100;
      progreso = progreso / total_actas;
      barraProgreso.attr("style", "width:" + progreso + "%");
      barraProgreso.attr("class", "progress-bar");
      $("#progreso").text("" + progreso + "% de actas capturadas");

      // Peticion para obtener el conteo de actas
      return $.ajax({
        url: "conteo.php",
        type: "get",
        data: opcion,
        success: function (data) {
          capturados = data;
        },
        complete: function () {
          setTimeout(actualizaProgreso, tiempo);
        }
      });


    }// Final funcion actualizarProgreso

  }); // Final DOM listo


  // The rest of code goes here! (The DOM may not be ready)

}));
