<!DOCTYPE html>
<html lang="en">
<?php
	require_once("modelos/prep_model.php");
	require_once("modelos/votos_model.php");
	require_once("config/config.php");
	include 'header.php';
?>
<head>
    <meta charset="UTF-8">
	<title>Seguimiento de votos</title>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
		integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
		integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
        crossorigin="anonymous"></script>
    <script src="js/linea_captura.js" ></script>
    <link rel="stylesheet" href="css/styles.css">
		<!--Script Chart -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <!--Ingreso de estilos-->
    <style>
        h1 {
            text-align: center;
        }

        p {
            text-align: center
        }

        p2 {
            text-align: right;
        }

        div {
            text-align: right;
            width: 450px;
            margin-top: 0px;
        }

        label.centro {
            display: inline-block;
            width: 10px;
            margin-right: 117px;
        }

        td{
            padding:5px 5px 0px;
        }

    </style>


</head>

<body>
  <?php
  // Declaracion de variables
  $conteoTotal = new Votos_model(); // modelo para obtener votos
  $prep 			 = new Prep_model(); // modelo para obtener datos en general
  // Obtenemos el listado de candidatos
  $arrCandidatos = $prep->get_candidatos();
  // Obtenemos los votos por cada candidato
  $arrVotosCandidatos = $prep->get_votos_candidatos();
  $arrPartidos = $prep->get_partidos();
  $arrPorcentajeVotos = [];
  $total = $conteoTotal->get_total_votos();
  $nulos = $conteoTotal->get_votos_nulos();
  $final = $total + $nulos;
  ?>

<div class="col-sm-12 ">
<div class="col-sm-12 text-center"> <h2>Progreso Actual</h2></div>
<!-- Barra de progreso-->
    <div class="center-block">
        <div class="progress ">
        <div id="progress-bar" class="progress-bar" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="">
        <label id="progreso"></label>
        </div>
        </div>
    </div>
</div>

<div class="container center-block">
<table class="table table-responsive" style="table-layout: fixed;">
<tr >
	<?php foreach($arrVotosCandidatos as $candidato):?>
	<!-- Canvas de la grafica -->
	    <td>
        <canvas class="col-md-2" id="<?php echo $candidato['id_candidato']; ?>"></canvas>
        </td>
    <?php
		$votos=$candidato['votos'];
		$porcentaje = ($votos * 100) / $final;
		// porcentaje = votos por candidato * 100% entre el total de votos
        $arrPorcentajeVotos[] = array_merge($arrPorcentajeVotos,
				array("id_candidato"=> $candidato['id_candidato'],
				"porcentaje"=>round($porcentaje, 2, PHP_ROUND_HALF_ODD)));
	?>
	<?php endforeach; ?>
    <td>
        <!-- Grafica de votos nulos -->
        <canvas class="col-md-2" id="votos_nulos"></canvas>
    </td>
    <?php //Porcentaje de votos nulos
    $fin = ($nulos*100) / $final;
    $totalPorcentajeNulos = $fin;

    ?>


</tr>
<tr>
<?php foreach($arrVotosCandidatos as $candidato):?>
	<?php
		$votos=$candidato['votos'];
		$porcentaje = ($votos * 100) / $final;
		// porcentaje = votos por candidato * 100% entre el total de votos
	?>
	<td><p><b><?php echo $candidato['votos']." (".round($porcentaje, 2,
	PHP_ROUND_HALF_ODD).")"?> % </b></p></td>
<?php endforeach; ?>
<td><p><b><?php echo $conteoTotal->get_votos_nulos();
             $fin = ($nulos*100) / $final;
             echo " (".round($fin, 2, PHP_ROUND_HALF_ODD).")" ?> % </b></p></td>
</tr>
<tr>
<?php foreach($arrVotosCandidatos as $candidato):?>
	<td><p><?php echo utf8_encode ( $candidato['nombre_partido']); ?> </p></td>
<?php endforeach; ?>
<td><p><?php echo "-"; ?> </p></td>
</tr>
<tr>
<?php foreach ( $arrCandidatos as $candidato ):  ?>

	<th><p><?php echo utf8_encode($candidato['nombre']." ".$candidato['p_apellido']." ".
	$candidato['s_apellido']);?></p></th>

<?php endforeach; ?>
<th><p><b>VOTOS NULOS</b></p></th>
</tr>

</table>
</div>

<script>
// Chart options
 Chart.defaults.global.legend.display = false;///// y las 4 lineas de wrapper
 Chart.defaults.global.tooltips.enabled = false;/////

var options = {
scales: {
    xAxes: [{
                display: false
            }],
    yAxes: [{
                display: false,
								ticks: {
                suggestedMin: 0,    // minimum will be 0, unless there is a lower value.
				suggestedMax: 100,

            }
            }]
    }
};

// Declaracion de cada grafica
<?php foreach ( $arrCandidatos as $candidato ):?>
var <?php echo "candidato".$candidato['id_candidato']; ?> =
document.getElementById("<?php echo $candidato['id_candidato']; ?>");
<?php endforeach;?>

<?php foreach ( $arrPorcentajeVotos as $candidato ):?>
var <?php echo "myChart".$candidato['id_candidato']; ?> = new
Chart(<?php echo "candidato".$candidato['id_candidato']; ?>, {
responsive: true,
type: 'bar',
data: {
  labels: [],
  datasets: [{
        data: [<?php echo $candidato['porcentaje'];?>],
        backgroundColor: [
            'rgba(255, 99, 132, 0.2)'
        ],
        borderColor: [
            'rgba(255,99,132,1)'
        ],
        borderWidth: 1,
				scaleStepWidth : 100,
        scaleStartValue : 0
    }]
  },
options: options
});
<?php endforeach;?>
var chartNulos = document.getElementById("votos_nulos");
var myChartNulos = new Chart(chartNulos, {
responsive: true,
type: 'bar',
data: {
  labels: [],
  datasets: [{
        data: [<?php echo $totalPorcentajeNulos;?>],
        backgroundColor: [
            'rgba(255, 99, 132, 0.2)'
        ],
        borderColor: [
            'rgba(255,99,132,1)'
        ],
        borderWidth: 1,
				scaleStepWidth : 100,
        scaleStartValue : 0
    }]
  },
options: options
});


</script>
</body>
</html>
