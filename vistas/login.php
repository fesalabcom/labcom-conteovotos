<?php
include 'header.php';
 ?>
 <title>Login</title>

<body class="text-center">
  <form  action="selecciona_casilla.php" class="form-signin" id="login" method="POST">
    <img class="mb-4" src="src/FES.png" alt="" width="200" height="200">
    <h1 class="h3 mb-3 font-weight-normal">Acceso al registro de votos</h1>
    <center>
    <div>
    <label class="sr-only">Nombre de usuario</label>
    <input type="text" name="usr" id="usr" class="form-control" placeholder="Nombre de usuario" required autofocus>
    <br/>
    <label class="sr-only">Contraseña</label>
    <input type="password" name="cont" id="cont" width="100" class="form-control" placeholder="Contraseña" required>
    <br/>

    <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
  </div>
    <p class="mt-5 mb-3 text-muted">&copy;Team_lab_com_2018</p>
  </form>
</body>
<!-- Ingreso de estilos -->
<style>
        h1
        {
            text-align: center;
        }
        #fesimg
        {
            float: right;
        }
        p
        {
            text-align: center
        }
        p2
        {
            text-align: right 520px;
        }
        div
        {
            text-align: right;
            width: 450px;
            margin-top: 0px;
        }
        label.centro
        {
            display: inline-block;
            width: 10px;
            margin-right: 117px;
        }
        input.centro[type="password"]{
            margin-bottom: 0px;
        }
    </style>
</html>
