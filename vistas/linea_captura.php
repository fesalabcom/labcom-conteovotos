<!DOCTYPE html>
<html>
<?php
include 'header.php';
 ?>
<head>


	<nav class="navbar navbar-inverse">
		<div class="container-fluid">
			<div class="navbar-header">
				<a class="navbar-brand" href="#">VOTOS_2018</a>
 			</div>
			<ul class="nav navbar-nav">
				<li class="active"><a href="#">Información</a></li>
				<li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Consulta</a></li>
			</ul>
			<ul class="nav navbar-nav navbar-right">
	 		
 		<li><a href="login.php"><span class="glyphicon glyphicon-log-in"></span> Salir</a></li>
 			</ul>
		</div>
	</nav>

<meta charset="UTF-8">

<title>Sistema de Registro de Actas</title>
</head>

<body >

<h2>Progreso Actual</h2>
<div class="progress">
    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width:80%">
    <label >Actas Capturadas: 80/100 (80%)</label>
    </div>
</div>
<div><b>PartidoA:  A votos (A%) </b></div>
<div><b>PartidoB:  B votos (B%) </b></div>
<div><b>PartidoC:  C votos (C%) </b></div>
<h2>Formulario de Captura de Actas</h2>
   <div class="container">
	<form method="post" >

		<div class="form-inline">
			 <label >PartidoA: </label>
			 <input type="number" class="form-control" placeholder="Ingrese numero" name="votosA"
							id="votosA"/>
			 <input type="checkbox" name="sin_datosA" id="sin_datosA" onclick="clickaction(this)"/>
			 <label for="sin_datos">Sin Datos</label>
		</div>

	<div class="form-inline">
	   <label>PartidoB: </label>
		 <input type="number" class="form-control" placeholder="Ingrese numero" name="votosB"
						id="votosB"/>
	   <input type="checkbox" name="sin_datosB" id="sin_datosB" onclick="clickaction(this)" />
	   <label for="sin_datos">Sin Datos</label>
	</div>

	<div class="form-inline">
	   <label>PartidoC: </label>
		 <input type="number" class="form-control" placeholder="Ingrese numero" name="votosC"
						id="votosC"/>
	   <input type="checkbox" name="sin_datosC" id="sin_datosC" onclick="clickaction(this)"/>
	   <label for="sin_datos">Sin Datos</label>
	</div>

    <!-- ENVIAR DATOS -->
	<button type="button" class="btn btn-success btn-md" aria-label="Left Align">
    <span class="glyphicon glyphicon-ok " aria-hidden="true"></span> ENVIAR DATOS
    </button>
    <!-- ACTA ILEGIBLE -->
    <button type="button" onclick="activar()" class="btn btn-warning btn-md" aria-label="Left Align">
    <span class="glyphicon glyphicon glyphicon-exclamation-sign " aria-hidden="true"></span> ACTA ILEGIBLE
    </button>

	</form>
</div>

</body>
<footer>
  <p>&copy; Creado por: Team_Labcom enero 2018</p>
</footer>



</html>
