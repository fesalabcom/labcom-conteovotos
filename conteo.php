<?php
/**
 * Consulta del conteo de actas al momento
 * @return capturados Actas Capturadas
 */
require_once('modelos/prep_model.php');
require_once('config/config.php');

$conteo = new Prep_model();

// Recepcion de peticiones 
$tipo_peticion = "";

$peticion = "";

$peticion = $_GET["peticion"];

$peticion = limpia_entrada($peticion);

switch ($peticion) {
    case 'total_actas':
            echo $conteo->total_actas;
        break;
    case 'capturados':
            echo $conteo->get_capturados();
        break;
    default:
            echo NULL;
        break;
}

?>