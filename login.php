<?php
require_once ('modelos/usuarios_model.php');
require_once ('config/config.php');
/**
 *
 * recibe datos del form
 * los compara con la base de datos
 * crea una sesion y  redirecciona
 * @MarcoDiaz
 */
 /**
  * [if description]
  *
  * @var [type]
  */

 if (isset ($_POST['submit']))
 {
   if (!empty($_POST['usr'])&& !empty($_POST['cont']))
   {
     $consulta = new Usuarios_model();
     $u = $_POST['usr'];
     $c = $_POST['cont'];

     $u = limpia_entrada( $u );
     $c = limpia_entrada( $c );

     $resultado = $consulta->get_Hash($u);
     /**
      *print_r($resultado);
      * @var [type]
      */
      // Si el resultado contiene datos
    if ($resultado){
    
     if (password_verify($c,$resultado[0]['contrasenia']))
     {
       //$nivel = $consulta->get_isActivo($resultado[0]['id_usuario']);
       session_start(); // Creamos Una Sesión

        $_SESSION['login_user']=$resultado[0]['usuario'];
        $_SESSION['tipo_usuario']=$resultado[0]['id_tipo_acceso'];

        header("location: selecciona_casilla.php");
    
     } 
    }

    $error ='Usuario ó Contraseña Invalidos';

   } else {
     $error ='Llena los campos, solicitados';
   }
 }

 /**
  * [Modelo de Usuarios para la conexión de
  * la base de datos y hacer el match]
  * @var [type]
  *
*/
 ?>
 <HTML5>
 <head>
     <meta charset="UTF-8">
     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
     <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
     integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
         crossorigin="anonymous">
     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
     integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
         crossorigin="anonymous"></script>
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Login</title>
     <!--Ingreso de estilos-->
     <style>
         #fesimg {
             float: right;
         }
         div.container {
             text-align: center;
             width: 450px;
             margin-top: 0px;
         }
     </style>
 </head>
<body class="text-center">
  <form   class="form-signin" id="login" method="POST">
    <img class="mb-4" src="vistas/src/FES.png" alt="" width="200" height="200">
    <h1 class="h3 mb-3 font-weight-normal">Acceso al registro de votos</h1>
    <center>
    <div class="container">
      <label class="sr-only">Nombre de usuario</label>
      <input type="text" name="usr" id="usr" class="form-control"
      placeholder="Nombre de usuario" required autofocus>
      <br/>
      <label class="sr-only">Contraseña</label>
      <input type="password" name="cont" id="cont" width="100"
      class="form-control" placeholder="Contraseña" required>
      <br/>
      <button class="btn btn-lg btn-primary btn-block"
      type="submit" name="submit">Ingresar</button>
  <?php if(isset($error)):?>
  <div class="alert alert-danger" role="alert"><?php echo $error; ?></div>
        <?php endif; ?>
  </div>

  </form>

</body>
  <footer>
    <p class="mt-5 mb-3 text-muted">&copy;Team_lab_com_2018</p>
  </footer>
</html>
