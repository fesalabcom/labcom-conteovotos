<?php
/**
 * Clase para realizar Altas, Bajas y Cambios en la BD
 *
 * Consejos de seguridad tomadas del manual de php
 * @see http://php.net/manual/es/security.database.sql-injection.php"
 *
 * @author Ulises Escamilla
 * @version 1.0.0
 * @category bases de datos
 */
require_once('config/config.php');


 class Prep_model{

    /**
     * Variable de la clase de conexión a la BD
     *
     * @var [conexion]
     */
    private $conexion;

    /**
     * Total de actas a capturas/esperadas
     *
     * @var [total_actas]
     */
    public $total_actas = 0;

    /**
     * Al instanciar la clase, realizamos la conexión
     */
    function __construct(){

        try{
            $this->conexion = new PDO(
                'mysql:host='.DB_HOST.';'.
                'dbname='.DB_NAME,
                DB_USER,
                DB_PASSWORD
            );
        }catch(PDOException $e){
            genera_log('ERROR AL CONECTAR A LA BASE DE DATOS: '.$e->getMessage());
            die("Conexión a la Base de Datos fallida ");
        }

        // Obtenemos el total de actas esperadas
        $stmt = $this->conexion->prepare(
            'SELECT COUNT(id_casilla) as total_actas FROM acta_por_casillas LIMIT 1'
        );
        $stmt->execute();
        $this->total_actas = $stmt->fetchColumn();

    }

    /**
     * Método para consultar candidatos activos id - nombre completo - partido
     *
     * @return Array candidatos
     */
    public function get_candidatos(){
        $stmt = $this->conexion->prepare(
            'SELECT
            id_candidato,
            UPPER(nombre) as nombre,
            UPPER(p_apellido) as p_apellido,
            UPPER(s_apellido) as s_apellido,
            id_partido
             FROM candidatos WHERE activo = 1'
        );
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        $out = $stmt->fetchAll();

        return $out;
    }

    /**
     * Método para consultar partidos activos
     *
     * @return Array id - nombre de los partidos
     */
    public function get_partidos(){
        $stmt = $this->conexion->prepare(
            'SELECT id_partido, nombre FROM c_partidos WHERE activo = 1'
        );
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_ASSOC);
        $out = $stmt->fetchAll();

        return $out;
    }


    /**
     * Consulta las actas capturadas
     *
     * @return Int Número de actas capturadas en el momento
     */
    public function get_capturados(){
        $stmt = $this->conexion->prepare(
            'SELECT COUNT(id_casilla) as actas_capturadas
            FROM acta_por_casillas WHERE contabilizada = 1 LIMIT 1'
        );

        $stmt->execute();
        $num_actas = $stmt->fetchColumn();
        return $num_actas;
    }

    /**
     * Obtiene un array de los votos por candidato
     *
     * @return Array Asociativo de votos por candidato
     */
    public function get_votos_candidatos(){
        $stmt = $this->conexion->prepare(
            'SELECT
                candidatos.id_candidato,
                SUM(votos) as votos,
                UPPER(candidatos.nombre) as nombre,
                UPPER(candidatos.p_apellido) as p_apellido,
                UPPER(candidatos.s_apellido) as s_apellido,
                UPPER(c_partidos.nombre) as nombre_partido
            FROM planilla
            INNER JOIN candidatos ON planilla.id_candidato = candidatos.id_candidato
            INNER JOIN c_partidos ON candidatos.id_partido = c_partidos.id_partido
            WHERE candidatos.activo = 1
            GROUP BY planilla.id_candidato'
        );
        $stmt->execute();

        $stmt->setFetchMode(PDO::FETCH_ASSOC);

        return $stmt->fetchAll();

    }


    /**
     * Corrobora si el id indicado es un candidato en la BD
     *
     * @param int $idCandidato el ID del candidato
     * @return boolean Si el candidato es valido
     */
    public function is_candidato( $idCandidato ){
        $idCandidato = limpia_entrada( $idCandidato );

        $stmt = $this->conexion->prepare(
            'SELECT activo
            FROM candidatos
            WHERE id_candidato = :idCandidato'
        );

        $stmt->bindParam(':idCandidato', $idCandidato);

        $stmt->execute();

        if($stmt->fetchColumn()){
            return TRUE;
        }
        else{
            return FALSE;
        }
    }

    /**
     * Obtiene un arreglo de las casilla no contabilizadas
     *
     * @return Array Arreglo de casillas no contabilizadas
     */
    public function get_casillas(){
            $stmt = $this->conexion->prepare(
                'SELECT id_casilla FROM acta_por_casillas WHERE contabilizada=0'
            );
            $stmt->execute();

            $stmt->setFetchMode(PDO::FETCH_ASSOC);

            return $stmt->fetchAll();
        }

        /**
         * Corrobora si el id de la casilla esta contabilizada
         *
         * @param int $idCasilla el ID de la casilla
         * @return boolean Si la casilla esta contabilizada
         */

        public function is_contabilizada( $idCasilla ){
            $idCasilla= limpia_entrada( $idCasilla );

            $stmt = $this->conexion->prepare(
                'SELECT id_casilla
                FROM acta_por_casillas
                WHERE id_casilla=:id_casilla
                AND contabilizada=1'
            );

            $stmt->bindParam(':id_casilla', $idCasilla);

            $stmt->execute();

            if (is_string($stmt->fetchColumn())){
                return TRUE;
            }else{
                return FALSE;
            }
        }

        /**
         * Corrobora si el id de la casilla existe en la BD
         *
         * @param int $idCasilla el ID de la casilla
         * @return boolean Si la casilla existe
         */
        public function existe_casilla($idCasilla){
            $idCasilla= limpia_entrada( $idCasilla );

            $stmt = $this->conexion->prepare(
                'SELECT id_casilla
                FROM acta_por_casillas
                WHERE id_casilla=:id_casilla'
            );

            $stmt->bindParam(':id_casilla', $idCasilla);

            $stmt->execute();

            if (is_string($stmt->fetchColumn())){
                return TRUE;
            }else{
                return FALSE;
            }
        }
 }
?>
