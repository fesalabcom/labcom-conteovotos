<?php
/**
 * Clase para realizar la captura de una acta en la BD
 *
 * Consejos de seguridad tomadas del manual de php
 * @see http://php.net/manual/es/security.database.sql-injection.php"
 *
 * @author Ulises Escamilla
 * @version 1.0.0
 * @category bases de datos; votación
 */
require_once('config/config.php');


 class Votos_model{

    /**
     * Variable de la clase de conexión a la BD
     *
     * @var [conexion]
     */
    private $conexion;

    /**
     * Total de actas a capturas/esperadas
     *
     * @var [total_actas]
     */
    public $total_actas = 0;

    /**
     * Al instanciar la clase, realizamos la conexión
     */
    function __construct(){

        try{
            $this->conexion = new PDO(
                'mysql:host='.DB_HOST.';'.
                'dbname='.DB_NAME,
                DB_USER,
                DB_PASSWORD
            );
        }catch(PDOException $e){
            genera_log('ERROR AL CONECTAR A LA BASE DE DATOS: '.$e->getMessage());
            die("Conexión a la Base de Datos fallida ");
        }

        // Obtenemos el total de actas esperadas
        $stmt = $this->conexion->prepare(
            'SELECT COUNT(id_casilla) as total_actas FROM acta_por_casillas LIMIT 1'
        );
        $stmt->execute();
        $this->total_actas = $stmt->fetchColumn();

    }

    /**
     * Función en el flujo básico para marcar un acta como ilegible
     *
     * @return Int Registros guardados
     */
    function set_casilla_ilegible( $idCasilla ){
        // Marca como -NO- legible y contabilizada
        $stmt = $this->conexion->prepare("UPDATE acta_por_casillas SET legible=0, contabilizada=1 WHERE id_casilla=:id_casilla");

        $stmt->bindParam(':id_casilla', $idCasilla );

        $stmt->execute();

        return $stmt->rowCount();
    }

    /**
     * Obtiene el total de votos validos de la suma de cada candidato
     *
     * @return Int Total de votos validos
     */
    function get_total_votos(){
        // Toma el TOTAL de votos independientemente del candidato
        $stmt = $this->conexion->prepare("SELECT SUM(votos) AS total_votos FROM planilla LIMIT 1");
        $stmt->execute();
        $num_actas = $stmt->fetchColumn();
        return $num_actas;
    }

    /**
     * Realiza el ingreso de los votos al candidato especificado
     *
     * @param String $id_Candidato
     * @param Int $votos votos dados al candidato
     * @param Boolean $conDatos Bandera en caso de que no haya datos
     * @return Int Registros guardados
     */
    function set_voto_candidato( $idCandidato ,$idCasilla, $votos, $conDatos){
        $stmt = $this->conexion->prepare("INSERT INTO planilla
            (id_candidato, id_casilla, votos, con_datos)
            VALUES (:idCandidato, :idCasilla, :votos, :conDatos)");

        $stmt->bindParam(':idCandidato'   , $idCandidato );
        $stmt->bindParam(':idCasilla'     , $idCasilla );
        $stmt->bindParam(':votos'         , $votos );
        $stmt->bindParam(':conDatos'      , $conDatos );

        $stmt->execute();

        return $stmt->rowCount();

    }


    /**
     * Wrapper de set_voto_candidato para el caso de sin datos
     *
     *
     * @param String $idCandidato
     * @param String $idCasilla
     * @return Int Número de registros afectados
     */
    function set_sin_datos( $idCandidato, $idCasilla ){

        return $this->set_voto_candidato( $idCandidato, $idCasilla, 0, 0 );

    }


    /**
     * Registra votos nulos del acta
     *
     * @param String $idCasilla capturada
     * @param Int $votos nulos capturados
     * @return Int Número de registros afectados
     */
    function set_votos_nulos( $idCasilla, $votosNulos ){

        $stmt = $this->conexion->prepare("UPDATE acta_por_casillas
        SET votos_nulos = :votos_nulos
        WHERE id_casilla = :id_casilla");

        $stmt->bindParam(':votos_nulos'   , $votosNulos );
        $stmt->bindParam(':id_casilla'   , $idCasilla );

        $stmt->execute();

        return $stmt->rowCount();
    }
    
    /**
     * Obtiene el total de votos nulos capturados en las actas hasta el momento
     *
     * @return Int $total_votos_nulos capturados
     */
    function get_votos_nulos(){

        $stmt = $this->conexion->prepare("SELECT SUM(votos_nulos) AS total_votos_nulos 
        FROM acta_por_casillas LIMIT 1");

        $stmt->execute();
        $num_actas = $stmt->fetchColumn();
        return $num_actas;
    }

    /**
     * Cambia el estado de una casilla a contabilizada
     *
     * @param String $idCasilla
     * @return Int Número de registros afectados
     */
    function set_casilla_contabilizada( $idCasilla ){

        $stmt = $this->conexion->prepare("UPDATE acta_por_casillas
        SET legible=1, contabilizada = 1
        WHERE id_casilla = :id_casilla");

        $stmt->bindParam(':id_casilla'   , $idCasilla );
        $stmt->execute();
        return $stmt->rowCount();
    }

 }



 ?>
