<?php
/**
 * Clase para realizar Altas, Bajas y Cambios de los usuarios
 *
 * Consejos de seguridad tomadas del manual de php
 * @see http://php.net/manual/es/security.database.sql-injection.php"
 *
 * @author Ulises Escamilla
 * @version 1.0.0
 * @category bases de datos
 */
require_once('config/config.php');


 class Usuarios_model{

    /**
     * Variable de la clase de conexión a la BD
     *
     * @var [conexion]
     */
    private $conexion;

    /**
     * Al instanciar la clase, realizamos la conexión
     */
    function __construct(){
        try{
            $this->conexion = new PDO(
                'mysql:host='.DB_HOST.';'.
                'dbname='.DB_NAME,
                DB_USER,
                DB_PASSWORD
            );

            // PDO lanzará una excepción PDOException y establecerá sus propiedades
            // para luego poder reflejar el error y su información.
            $this->conexion->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        }catch(PDOException $e){
            genera_log('ERROR AL CONECTAR A LA BASE DE DATOS: '.$e->getMessage());
            die("Conexión a la Base de Datos fallida ");
        }
    }


    /**
     * Método para generar un nuevo usuario en la BD
     *
     * @param [String] $usuario
     * @param [String] $password
     * @param [String] $activo_inicio fecha y hora de inicio de vida en formato yyyy-mm-dd hh:mm
     * @param [String] $activo_fin fecha y hora de fin de vida en formato yyyy-mm-dd hh:mm
     * @param [String] $tipo_acceso como entero segun el catalogo de acceso
     * @return mixed False si no se ejecuto el ingreso o el numero de filas afectadas
     */
    public function set_nuevo_usuario(
       $usuario, $password, $activo_inicio, $activo_fin, $tipo_acceso )
    {
        // limpiamos datos
        $usuario = limpia_entrada( $usuario );
        $password = limpia_entrada( $password );

        // realizamos el hash al password
        $password = password_hash($password, PASSWORD_DEFAULT);

        // Corroboramos que las fechas sean validas

        if(isset($activo_inicio) || isset($activo_fin) ){
            if( !is_valid_date($activo_inicio) ){
                return FALSE;
            }

            if( !is_valid_date($activo_fin) ){
                return FALSE;
            }
        }


        // Corroboramos que el tipo sea entero
        if (!is_int( $tipo_acceso )){
            return FALSE;
        }

        $stmt = $this->conexion->prepare("INSERT INTO usuarios
            (usuario, contrasenia, acceso_inicio, acceso_fin, id_tipo_acceso, activo)
            VALUES (:usuario, :contrasenia, :acceso_inicio, :acceso_fin, :id_tipo_acceso, 1 )");

        $activo_inicio = $activo_inicio;
        $activo_fin = $activo_fin;

        $stmt->bindParam(':usuario'         , $usuario );
        $stmt->bindParam(':contrasenia'     , $password );
        $stmt->bindParam(':acceso_inicio'   , $activo_inicio );
        $stmt->bindParam(':acceso_fin'      , $activo_fin );
        $stmt->bindParam(':id_tipo_acceso'  , $tipo_acceso );

        $stmt->execute();

        return $stmt->rowCount();

    }
    public function set_acceso($nombre){
      $query=$this->conexion->prepare("INSERT INTO c_accesos (nombre) VALUES (:nombre)");
      $nombre = limpia_entrada($nombre);
      $query->bindParam(':nombre'  , $nombre);
      $query->execute();
      return $query->rowCount();
      
    }
    /**
     * Recibe el user lo busca y trae el usuario
     * y la contraseña (el hash)
     * @param  [type] $user [usuario del login ]
     * @return [type]       [regresa un asociativp]
     */
    public function get_Hash($user){
        try{
            $query = ("SELECT usuario, contrasenia, id_usuario, id_tipo_acceso
              FROM usuarios WHERE usuario = '$user' AND activo=1" );
            $res = $this->conexion->prepare($query);
            $res->execute();
            $res->setFetchMode(PDO::FETCH_ASSOC);
            //asignar a una variable y retornar con indice cero
            return $res->fetchAll();
        }catch(PDOException $e){
            header("location: login.php");
        }
    }
    /**
     * A partir del id trae si  es activo y su nivel de
     * acceso a la plataforma ,
     * @param  [int]  $ID [id del usuario]
     * @return      [description]
     */
    public function get_isActivo($ID){
      $query = ("SELECT id_tipo_acceso, activo
        FROM usuarios WHERE id_usuario ='$ID'");
      $res = $this->conexion->prepare($query);
      $res->execute();
      $res->setFetchMode(PDO::FETCH_ASSOC);
      return $res->fetchAll();

    }
 }

 ?>
