<?php
/**
 * Script encargado de guardar los votos generados por la linea de captura
 *
 * @author Ulises Escamilla
 * @version 1.0.0
 *
 */

require_once("modelos/prep_model.php");
require_once("modelos/votos_model.php");
require_once("config/config.php");


// Corroboramos datos enviados por POST

// Sin datos
if ( !$_POST ){
    // Aqui podemos redirigir a otro sitio si queremos
    header('location: login.php');
}

// Declaracion de variablesa
$boolLegible = 0;
$mensaje = "";
$regModificados = 0;

$boolLegible = limpia_entrada( $_POST['legible'] );



/************************************
 ********** Acta Ilegible ***********
 ************************************/
if ( !$boolLegible ){

    $idCasilla = $_POST['id_casilla'];
    $idCasilla = limpia_entrada( $idCasilla );

    $votos = new Votos_model();


    $mensaje .= "Casillas Contabilizadas como ilegible: ";
    $mensaje .= $votos->set_casilla_ilegible( $idCasilla );

    echo "<br>ID: ".$idCasilla;
    echo $mensaje;
    die();
}

// Obtener los resultados
$todoPost =  $_POST;

$prep = new Prep_model();


$datos = 0;

/************************************
 ********* Captura de Votos *********
 ************************************/

// Para cada campo que recibimos
foreach ($todoPost as $id => $valor) {

    // Corroborar si es un ID de candidato
    if( $prep->is_candidato($id) ){
        // Corroboramos que el voto sea numerico
        $valor = limpia_entrada( $valor );
        $valor = intval($valor);
        // Ingresamos el voto de cada candidato de la acta seleccionada
        if ( is_int( $valor ) && $valor != "" ){
            // $valor = al voto
            echo "Candidato : (".$id.")   - con votos: ".$valor."</br>";
            $votos = new Votos_model();
            $regModificados += $votos->set_voto_candidato($id , $todoPost['id_casilla'], $valor,  1);
        }elseif( $valor === "" ){
            $votos = new Votos_model();
            $regModificados += $votos->set_sin_datos( $id, $todoPost['id_casilla']);
            echo "Candidato : (".$id.") - Sin Datos</br>";
        }
    }
}

/************************************
 *********** Votos Nulos ************
 ************************************/

 $votos_nulos = $_POST['votos_nulos'];

 $votos = new Votos_model();

 $regModificados += $votos->set_votos_nulos($todoPost['id_casilla'], $todoPost['votos_nulos']);

 echo "Votos nulos: ".$todoPost['votos_nulos']."</br>";

 echo "Registros capturados: ".$regModificados;
 
/************************************
 ****** Casilla contabilizada *******
 ************************************/

 $casilla = new Votos_model();

 $casilla->set_casilla_contabilizada($todoPost['id_casilla']);

?>
