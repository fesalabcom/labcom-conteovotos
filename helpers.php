<?php
/**
 * Funciones de ayuda para todo el entorno en el proyecto
 * 
 * @version 1.0.0
 */
require_once('config/config.php');


 /**
  * Guarda el mensaje indicado en el log del sistema web si la configuración
  * lo permite
  *
  * @param string $mensaje a guardar en el log
  */
 function genera_log( $mensaje ){

    // Bandera para guardar o no el log
    if( GENERA_LOG ){
        error_log( $mensaje , 0);
    }
 }

 /**
  * Limpia un texto para realizar una consulta en sql
  * 
  * @param String $data de cualquier texto
  * @return String Los datos ya filtrados
  */
  function limpia_entrada( $data ){
      // Quitamos espacios
      $data = trim( $data );
      // Quitamos etiquetas
      $data = strip_tags( $data );

      return $data;
  }

  /**
   * Corrobora que sea una fecha valida en formato yyyy-mm-dd
   *
   * @param [String] $fecha a corroborar
   * @return boolean
   */
  function is_valid_date( $fecha ){
    if (preg_match("/^[0-9]{4}-(0[1-9]|1[0-2])-(0[1-9]|[1-2][0-9]|3[0-1])$/",$fecha)) {
        return (bool)strtotime($fecha);
    } else {
        return false;
    }
    
  }

?>
