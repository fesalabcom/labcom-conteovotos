# Linea de captura para conteo de votos de profesores

#Distribución del proyecto

## DB/
Archivos workbench para el diseño de la BD.

## Diagrama
![alt text][logo]

[logo]: db/imagen-db.png "Diseño base de datos"

## Config/
Constantes de configuración.

### database.php
Constantes para ingresar las credenciales en la BD

## Modelos/
Archivos de consulta de la base de datos.
* prep_model.php Consulta las tablas para el seguimiento del conteo.
* usuarios_mode.php Altas, Bajas y Cambios para los usuarios del sistema.

## Vistas/
Páginas sin funcionalidad backend, solo muesta la vista de la página y demuestra el flujo de trabajo esperado.


