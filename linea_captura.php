﻿<?php
// Corroborar session, si no redirigir al login
?>

<!DOCTYPE html>
<html>
<?php
include 'header.php';
include('tipo_sesion.php');
 ?>

 <?php
	require_once("modelos/prep_model.php");
	require_once("config/config.php");
  $prep = new Prep_model();

if ( !limpia_entrada( $_POST['id_casilla'] )){
  header("location: selecciona_casilla.php");
}

if ( !$prep->existe_casilla($_POST['id_casilla']) ){
  header("location: selecciona_casilla.php");
}
	// Obtencion del id de la casilla anteriormente seleccionada ||

	if ( $prep->is_contabilizada($_POST['id_casilla'])){
    header("location: selecciona_casilla.php");
	}

	$idCasilla = $_POST['id_casilla'];

	$idCasilla = limpia_entrada($idCasilla);



	$arrCandidatos = $prep->get_votos_candidatos();
	$arrPartidos = $prep->get_partidos();
  	$arrCandidatosActivos = $prep->get_candidatos();
 ?>


<body >
<p></p>
 <h2 align="center">Progreso Actual</h2>
<!-- Barra de progreso -->
 <div class="col-sm-5"></div>
 <div class="progress">
    <div id="progress-bar" class="progress-bar" aria-valuenow="" role="progressbar" aria-valuemin="0" aria-valuemax="100" style="">
    	<label id="progreso"></label>
    </div>
  </div>
  <!-- esto se debe pasar a otra pagina
<?php foreach ( $arrCandidatos as $candidato ):?>
<div><b> <?php echo utf8_encode($candidato['nombre_partido']); ?> Candidato:  <?php echo utf8_encode($candidato['nombre']." ".$candidato['p_apellido']." ".$candidato['s_apellido']); ?> votos ( <?php echo $candidato['votos']; ?> ) </b></div>
<?php endforeach; ?>-->
<div class="col-sm-2"></div>
<h2>Formulario de Captura de Actas</h2>


   <!-- Formulario -->
	<form action="captura_votos.php" method="post">
    <div id="contenedor"class="col-sm-12" >
  	<!-- Candidatos -->
      <?php  foreach ($arrCandidatosActivos as $candidato): ?>
      <div class="col-sm-5">
      <?php echo utf8_encode($candidato['nombre']." ".$candidato['p_apellido']." ".$candidato['s_apellido']); ?>
      </div>
      <div class="col-sm-3">
       <input type="number" class="form-control" placeholder="Ingrese dato" name="<?php echo $candidato['id_candidato'];?>"
              id="candidato<?php echo $candidato['id_candidato'];?>" min="0" required/>
       </div>
         <div class="col-sm-1">
       <input type="checkbox" class="checkbox" name="<?php echo $candidato['id_candidato']?>"
       id="<?php echo $candidato['id_candidato'];?>" value="0" />
</div>
<div class="col-sm-1">
  <label for="<?php echo $candidato['id_candidato'];?>" required>Sin Datos</label>
</div>
        <?php   endforeach;?>


<div class="col-sm-5">
      VOTOS NULOS
</div>
  <!-- Votos Nulos -->
<div class="col-sm-3">
    <input type="number" name="votos_nulos" class="form-control" placeholder="Ingrese dato"  id="candidatoNulo"/>
</div>


  	<input type="hidden" id="legible" name="legible" value="1">
  	<input type="hidden" id="id_casilla" name="id_casilla" value="<?php echo $idCasilla; ?>">
<div class="col-sm-12">
      <center>
    <!-- ENVIAR DATOS -->
	  <button type="submit" class="btn btn-success btn-md" aria-label="Left Align" id="btnEnviar">
    <span class="glyphicon glyphicon-ok " aria-hidden="true"></span> ENVIAR DATOS
    </button>
    <!-- ACTA ILEGIBLE -->
    <button type="button"  class="btn btn-warning btn-md" aria-label="Left Align" id="btnIlegible">
    <span class="glyphicon glyphicon glyphicon-exclamation-sign " aria-hidden="true"></span> ACTA ILEGIBLE
    </button>
</div>
	</form>

</body>
<footer><div class="col-sm-12">
  <p>&copy; Creado por: Team_Labcom enero 2018</p></div>
</footer>

</html>
